Como executar a API:

Executar através do SpringBoot

Para executar os testes basta executar um maven build com o parametro test.

A API esta configrada para rodar na porta 8090, caso queira trocar basta alterar o arquivo .properties.

Console H2 (http://localhost:8090/h2)

Usuario h2 – login

Senha h2 – senha

Caso queira trocar usuario e senha basta alterar o arquivo.properties.

--------------------------------------------------------------------------------------------------------------------------------------

Listar todas Persons:
	Method – GET
	http://localhost:8090/api/spread/persons
--------------------------------------------------------------------------------------------------------------------------------------
Listar Persons por Skill:
	Method – GET
	http://localhost:8090/api/spread/personsBySkill?skillId=?
--------------------------------------------------------------------------------------------------------------------------------------
Listar Person por Id:
	Method – GET
	http://localhost:8090/api/spread/person?personId=?
--------------------------------------------------------------------------------------------------------------------------------------
Salvar/Atualizar Person:
	Method - POST
	http://localhost:8090/api/spread/savePerson
	RequestBody:
	{
   		"id": 30, //somente em caso de update
   		"firstName": "update",
   		"lastName": "2",
   		"linkedInUrl": "2",
   		"whatsApp": "2",
   		"mail": "2",
   		"skills":[
   	       	{
   		      "id": 4,
         		"skillTag": null,
         		"skillDescription": null
      		}
		]
	}
--------------------------------------------------------------------------------------------------------------------------------------
Deletar Person por Id:
	Method - POST
	http://localhost:8090/api/spread/deletePerson?personId=?
--------------------------------------------------------------------------------------------------------------------------------------
Salvar/Atualizar Skill:
	Method - POST
	http://localhost:8090/api/spread/saveSkill
	RequestBody:
		{
		"id": 4, // somente em caso de update
       	"skillTag": “1”,
         	"skillDescription": “1”
		}
--------------------------------------------------------------------------------------------------------------------------------------
Listar Skill por Id:
	Method - GET
	http://localhost:8090/api/spread/skill
--------------------------------------------------------------------------------------------------------------------------------------
Listar Skills por Person
	Method - GET
	http://localhost:8090/api/spread/skillsByPerson?skillId=?
--------------------------------------------------------------------------------------------------------------------------------------
Listar todas Skills:
	Method - GET
	http://localhost:8090/api/spread/skills
--------------------------------------------------------------------------------------------------------------------------------------
Deletar Skill:
	Method - Put
	http://localhost:8090/api/spread/deleteSkill?skillId=?
--------------------------------------------------------------------------------------------------------------------------------------