package br.com.spread.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.spread.model.Person;
import br.com.spread.model.Skill;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {

	@Query("SELECT s FROM Skill s INNER JOIN s.persons p WHERE p.id=?")
	List<Skill> findSkillByPersonId(Long PersonId);
}
