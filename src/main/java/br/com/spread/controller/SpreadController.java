package br.com.spread.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.spread.model.Person;
import br.com.spread.model.Skill;
import br.com.spread.service.PersonService;
import br.com.spread.service.SkillService;

@RestController
@RequestMapping("/")
public class SpreadController {

	@Autowired private PersonService personService;
	@Autowired private SkillService skillService;
	
	public SpreadController() {
		super();
	}

	@RequestMapping(value = "/api/spread/persons", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Person>> getPersons() {
		return ResponseEntity.status(HttpStatus.OK).body(personService.getPersons());
	}

	@RequestMapping(value = "/api/spread/personsBySkill", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Person>> getPersonsBySkill(@RequestParam(value="skillId", required= true) Long skillId) {
		return ResponseEntity.status(HttpStatus.OK).body(personService.getPersonsBySkillId(skillId));
	}

	@RequestMapping(value = "/api/spread/person", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Person> getPerson(@RequestParam(value="personId", required= true) Long personId) {
		return ResponseEntity.status(HttpStatus.OK).body(personService.getPerson(personId));
	}

	@RequestMapping(value = "/api/spread/savePerson", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Person> savePerson(@RequestBody Person person) {
		return ResponseEntity.status(HttpStatus.OK).body(personService.savePerson(person));
	}

	@RequestMapping(value = "/api/spread/deletePerson", produces = "application/json", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Void> deletePerson(@RequestParam(value = "personId", required = true) Long personId) {
		personService.deletePerson(personId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/api/spread/saveSkill", produces = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Skill> saveSkill(@RequestBody Skill skill) {
		return ResponseEntity.status(HttpStatus.OK).body(skillService.saveSkill(skill));
	}

	@RequestMapping(value = "/api/spread/skill", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Skill> getSkill(@RequestParam(value = "skillId", required = true) Long skillId) {
		return ResponseEntity.status(HttpStatus.OK).body(skillService.getSkill(skillId));
	}

	@RequestMapping(value = "/api/spread/skillsByPerson", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Skill>> getSkillsByPerson(@RequestParam(value = "personId", required = true) Long personId) {
		return ResponseEntity.status(HttpStatus.OK).body(skillService.getSkillsByPersonId(personId));
	}

	@RequestMapping(value = "/api/spread/skills", produces = "application/json", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Skill>> getSkills() {
		return ResponseEntity.status(HttpStatus.OK).body(skillService.getSkills());
	}

	@RequestMapping(value = "/api/spread/deleteSkill", produces = "application/json", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Void> deleteSkill(@RequestParam(value = "skillId", required = true) Long skillId) {
		skillService.deleteSkill(skillId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
