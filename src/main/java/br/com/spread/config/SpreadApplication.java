package br.com.spread.config;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;
import org.springframework.web.WebApplicationInitializer;

@SpringBootApplication (scanBasePackages = { "br.com.spread" })
public class SpreadApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	private static final Logger log = LoggerFactory.getLogger(SpreadApplication.class);
	
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SpreadApplication.class);
		Environment env = app.run(args).getEnvironment();
		try {
			log.info(
					"\n----------------------------------------------------------\n\t"
							+ "Application "+ env.getProperty("spring.application.name") +" is running! Access URLs:\n\t" + "Local: \t\thttp://localhost:"+env.getProperty("server.port") +"\n\t"
							+ "External: \thttp://"+ InetAddress.getLocalHost().getHostAddress() +":"+ env.getProperty("server.port") +"\n----------------------------------------------------------");
		} catch (UnknownHostException e) {
			log.error("Erro SpreadApplication!");
		}
	}
}
