package br.com.spread.service;

import java.util.List;

import br.com.spread.model.Skill;

public interface SkillService {

	
	List<Skill> getSkills();
	List<Skill> getSkillsByPersonId(Long personId);
	void deleteSkill(long id);
	Skill saveSkill(Skill skill);
	Skill getSkill(long id);
}
