package br.com.spread.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.spread.model.Person;
import br.com.spread.repository.PersonRepository;
import br.com.spread.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService{

	@Autowired PersonRepository personRepository;
	
	@Override
	public List<Person> getPersons() {
		return personRepository.findAll();
	}

	@Override
	public void deletePerson(long id) {
		personRepository.delete(id);
	}

	@Override
	public Person savePerson(Person person) {
		return personRepository.save(person);
	}

	@Override
	public List<Person> getPersonsBySkillId(Long skillId) {
		return personRepository.findPersonBySkillId(skillId);
	}

	@Override
	public Person getPerson(Long personId) {
		return personRepository.findOne(personId);
	}

}
