package br.com.spread.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.spread.model.Person;
import br.com.spread.model.Skill;
import br.com.spread.repository.PersonRepository;
import br.com.spread.repository.SkillRepository;
import br.com.spread.service.SpreadService;

@Service
public class SpreadServiceImpl implements SpreadService{

	@Autowired SkillRepository skillRepository;
	@Autowired PersonRepository personRepository;
	
	@Override
	public List<Person> getPersons() {
		return personRepository.findAll();
	}

	@Override
	public void deletePerson(long id) {
		personRepository.delete(id);
	}

	@Override
	public Person savePerson(Person person) {
		return personRepository.save(person);
	}

	@Override
	public Skill getSkill(long id) {
		return skillRepository.findOne(id);
	}

}
