package br.com.spread.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.spread.model.Skill;
import br.com.spread.repository.SkillRepository;
import br.com.spread.service.SkillService;

@Service
public class SkillServiceImpl implements SkillService{

	@Autowired SkillRepository skillRepository;

	@Override
	public List<Skill> getSkills() {
		return skillRepository.findAll();
	}

	@Override
	public void deleteSkill(long id) {
		skillRepository.delete(id);
	}

	@Override
	public Skill saveSkill(Skill skill) {
		return skillRepository.save(skill);
	}

	@Override
	public Skill getSkill(long id) {
		return skillRepository.findOne(id);
	}

	@Override
	public List<Skill> getSkillsByPersonId(Long personId) {
		return skillRepository.findSkillByPersonId(personId);
	}


}
