package br.com.spread.service;

import java.util.List;

import br.com.spread.model.Person;

public interface PersonService {

	
	List<Person> getPersons();
	List<Person> getPersonsBySkillId(Long skillId);
	Person getPerson(Long personId);
	void deletePerson(long id);
	Person savePerson(Person person);
}
