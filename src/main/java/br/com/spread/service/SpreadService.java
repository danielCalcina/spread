package br.com.spread.service;

import java.util.List;

import br.com.spread.model.Person;
import br.com.spread.model.Skill;

public interface SpreadService {

	
	List<Person> getPersons();
	void deletePerson(long id);
	Person savePerson(Person person);
	Skill getSkill(long id);
}
