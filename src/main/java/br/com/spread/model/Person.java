package br.com.spread.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String firstName;
	private String lastName;
	private String linkedInUrl;
	private String whatsApp;
	private String mail;
	
	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.REFRESH })
	@JoinTable(name = "person_skill", joinColumns = {
			@JoinColumn(name = "person_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "skill_id", referencedColumnName = "id") })
	private List<Skill> skills;

	public long getId() {
		return id;
	}

	public Person setId(long id) {
		this.id = id;
		return this;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skillsId) {
		this.skills = skillsId;
	}

	public String getFirstName() {
		return firstName;
	}

	public Person setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Person setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public String getLinkedInUrl() {
		return linkedInUrl;
	}

	public Person setLinkedInUrl(String linkedInUrl) {
		this.linkedInUrl = linkedInUrl;
		return this;
	}

	public String getWhatsApp() {
		return whatsApp;
	}

	public Person setWhatsApp(String whatsApp) {
		this.whatsApp = whatsApp;
		return this;
	}

	public String getMail() {
		return mail;
	}

	public Person setMail(String mail) {
		this.mail = mail;
		return this;
	}

}
