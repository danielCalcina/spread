package br.com.test.spread;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import br.com.spread.config.Configuration;
import br.com.spread.config.SpreadApplication;
import br.com.spread.model.Person;
import br.com.spread.service.SpreadService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Configuration.class, SpreadApplication.class })
@WebAppConfiguration
public class SreadControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private SpreadService service;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void validate_savePerson() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("firstName", "2");
		params.add("lastName", "2");
		params.add("linkedInUrl", "2");
		params.add("whatsApp", "2");
		params.add("mail", "2");
		params.add("skills", "1,1;2,2;3,3;4,4");
		mockMvc.perform(post("/api/spread/savePerson/").params(params)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	}

	@Test
	public void validate_deletePerson() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		List<Person> persons = service.getPersons();
		params.add("personId", Long.toString(persons.get(0).getId()));
		mockMvc.perform(put("/api/spread/deletePerson/").params(params)).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void validate_getPerson() throws Exception {
		mockMvc.perform(get("/api/spread/persons/")).andDo(print()).andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	}
	
	@Test
	public void validate_updatePerson() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		List<Person> persons = service.getPersons();
		params.add("id", Long.toString(persons.get(0).getId()));
		params.add("firstName", "update");
		mockMvc.perform(post("/api/spread/savePerson/").params(params)).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	}
	
}
