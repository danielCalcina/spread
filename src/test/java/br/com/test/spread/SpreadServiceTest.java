package br.com.test.spread;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


import br.com.spread.config.Configuration;
import br.com.spread.config.SpreadApplication;
import br.com.spread.model.Person;
import br.com.spread.model.Skill;
import br.com.spread.service.SpreadService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={Configuration.class, SpreadApplication.class})
@WebAppConfiguration
public class SpreadServiceTest {

	@Autowired SpreadService service;
	
	@Test
	public void savePerson() {
		Person p = new Person();
		Skill s = new Skill();
		List<Skill> skills = new ArrayList<Skill>();
		s.setSkillDescription("2");
		s.setSkillTag("2");
		skills.add(s);
		
		p.setFirstName("2").setLastName("2").setLinkedInUrl("2").setMail("2").setSkills(skills).setWhatsApp("2");
		service.savePerson(p);
		
		//updatePerson
		p.setFirstName("update");
		service.savePerson(p);
	}
	@Test
	public void getPersons() {
		List<Person> response = service.getPersons();
		Assert.assertNotNull(response);
		Assert.assertFalse(response.isEmpty());
	}
	@Test
	public void deletePerson() {
		List<Person> response = service.getPersons();
		service.deletePerson(response.get(0).getId());
	}

}
