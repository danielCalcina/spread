package br.com.test.spread;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.spread.config.Configuration;
import br.com.spread.config.SpreadApplication;
import br.com.spread.model.Person;
import br.com.spread.model.Skill;
import br.com.spread.service.PersonService;
import br.com.spread.service.SkillService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { Configuration.class, SpreadApplication.class })
@WebAppConfiguration
public class SpreadTest {

	@Autowired
	PersonService personService;
	@Autowired
	SkillService skillService;

	@Test
	public void saveSkill() {
		Skill s = new Skill();
		s.setSkillDescription("1");
		s.setSkillTag("1");
		skillService.saveSkill(s);

		// updateSkill
		s.setSkillTag("update");
		skillService.saveSkill(s);
	}

	@Test
	public void getSkills() {
		List<Skill> response = skillService.getSkills();
		Assert.assertNotNull(response);
		Assert.assertFalse(response.isEmpty());
	}

	@Test
	public void getSkill() {
		List<Skill> response = skillService.getSkills();
		skillService.getSkill(response.get(0).getId());
	}

	@Test
	public void deleteSkill() {
		skillService.deleteSkill(skillService.getSkills().get(0).getId());
	}

	@Test
	public void savePerson() {
		Skill s = new Skill();
		s.setSkillDescription("1");
		s.setSkillTag("1");
		skillService.saveSkill(s);

		Person p = new Person();
		p.setFirstName("2").setLastName("2").setLinkedInUrl("2").setMail("2").setWhatsApp("2")
				.setSkills(skillService.getSkills());
		personService.savePerson(p);

		// updatePerson
		p.setFirstName("update");
		personService.savePerson(p);
	}

	@Test
	public void getPersons() {
		List<Person> response = personService.getPersons();
		Assert.assertNotNull(response);
		Assert.assertFalse(response.isEmpty());
	}

	@Test
	public void deletePerson() {
		personService.deletePerson(personService.getPersons().get(0).getId());
	}

	@Test
	public void getPersonsBySkillId() {
		personService.getPersonsBySkillId(skillService.getSkills().get(0).getId());
	}

	@Test
	public void getSkillsByPersonId() {
		skillService.getSkillsByPersonId(personService.getPersons().get(0).getId());
	}

	@Test
	public void getPerson() {
		personService.deletePerson(personService.getPersons().get(0).getId());
	}
}
